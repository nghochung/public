#ifndef _StackTrace_h_
#define _StackTrace_h_

#include <windows.h>
#include <DbgHelp.h>

void RegisterRefCountEvent();
void UnregisterRefCountEvent();

void WriteRefCountEvent(LPVOID in_pInstance, BOOL in_bIsAddRef);

#endif