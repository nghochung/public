#include "StackTrace.h"

#include <evntprov.h>
#include <WinBase.h>

// header generated from instrument manifest
#include "xsi_instrument.h"
#include <string>
#include <vector>

//using namespace XSI;

typedef std::vector<DWORD64> StackTrace;

REGHANDLE gEventRegHandle = NULL; 

void RegisterRefCountEvent()
{
	DWORD status = ERROR_SUCCESS;

	// Register event
	status = EventRegister(
		&xsiProviderGuid,      // GUID that identifies the provider
		NULL,               // Callback not used
		NULL,               // Context noot used
		&gEventRegHandle // Used when calling EventWrite and EventUnregister
		);

	SymInitialize( GetCurrentProcess(), NULL, TRUE );
}

void UnregisterRefCountEvent()
{
	DWORD status = ERROR_SUCCESS;

	status = EventUnregister( gEventRegHandle );
}

// Description:
//   Send out a RefCount change event, with the callstack attached as event data
//   in_pInstance: address of the reference-counted object
//   in_bIsAddRef: true if it's a reference count increment, false if it's decrement
void WriteRefCountEvent(LPVOID in_pInstance, BOOL in_bIsAddRef)
{
	StackTrace stackTrace(256);
	USHORT nbCaptured = RtlCaptureStackBackTrace( 0, 256, (PVOID*)stackTrace.data(), NULL );

	EVENT_DATA_DESCRIPTOR DataDescriptor[3];
	LPVOID pAddr = in_pInstance;
	EventDataDescCreate( &DataDescriptor[0], &pAddr, sizeof(PVOID) );

	UINT32 stackDepth = (UINT32)nbCaptured;
	EventDataDescCreate( &DataDescriptor[1], &stackDepth, sizeof(UINT32) );

	if ( stackDepth > 0 )
	{
		EventDataDescCreate( &DataDescriptor[2], stackTrace.data(), stackDepth * sizeof(DWORD64) );
	}
	else
		EventDataDescCreate( &DataDescriptor[2], NULL, 0 );

	EventWrite( gEventRegHandle, in_bIsAddRef==TRUE ? &AddRefEvent : &DecRefEvent, 3, DataDescriptor );
}
