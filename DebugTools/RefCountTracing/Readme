RefCountTracing tool

INTRODUCTION
------------------------------------
Reference counting is a mechanism commonly used for resource management 
in C++. The resource object keeps a count of the references to it held 
by all other parties. When the count reaches zero, the object can be 
destroyed. A major problem with this method is that miscounting is 
particularly hard to track down. The reference count can be incremented 
and decremented a zillion times by the object's owners; when there are 
less decrements than increments (because some function somewhere failed 
to decrement the count), we have a leak. 

This tool was created to help developers identify these culprit 
functions. The idea is to compare the number of increments and 
decrements made by a function during its execution. If the two numbers 
are not equal, the function is a probable culprit; "probable" because 
the function can have valid reason for permanently changing the 
reference count, e.g. when an owner establishes its shared ownership of 
the resource object. The tool keeps track of increments/decrements by 
listening to events triggered by the reference count change. Whenever 
the tool receives an increment/decrement event, it records down all the 
functions in the callstack, and marks each of them as having made an 
increment/decrement. At the end of the recording session, the tool 
tallies the total number of increments/decrements made by each function 
and identifies the possible culprits. 


For example, consider the increment/decrement totals in this call sequence:
  myApp::run()
    classA::initResource(): inc/dec = 1/0
    classA::execute()
      classB::useResource(): inc/dec = 5/5
      classC::useResource(): inc/dec = 3/2
	classA::deleteResource(): inc/dec = 0/1
	
execute() have 'indirectly' incremented the count 8 times, and 
decremented it 7 times, so it is a possible culprit. Similarly, run() is 
a possible culprit because its inc/dec = 9/8. In fact, these caller 
functions further up the callstack are usually the real culprit. They 
usually keep smart pointers which, in turn, call increment/decrement on 
their behalf. 

DIRECTORY STRUCTURE
------------------------------------
Instrument\
These files define the event provider and the event data. These need to 
be registered with the event logger (logman.exe) before starting the 
recording session. 


StrackTrace\
These are the helper function to record the stacktrace. The stacktrace 
is the data attached to the event, to be recorded by logman.exe during 
the recording session. 


example\
An example program to demonstrate how to provide events from 
reference-counted objects 


ProcessLog\
Program to process the event log and figure out the leak culprits

WIP\
Work in progress, including:
- a post-mortem event log processor
- a port of the tool to Python

INSTRUCTION
------------------------------------
As described in the introduction, there are three steps involved:
1. Instrument your code to generate increment/decrement events
2. Record the increment/decrement events during the operation that causes the leak. 
3. Process the event log to tally the total increments/decrements made by each function
We use the toolset provided by Microsoft ETW and Debug Help Library

Step 1:
- Add Instrument\xsi_instrument.h and xsi_instrument.rc to your project. 

- Add StackTrace\StackTrace.h and StackTrace.cpp to your project. 

- Call 3 functions in your code: 
  RegisterRefCountEvent(): to be called before the event recording starts. 
  
  UnregisterRefCountEvent(): to be called after the recording ends. 
  
  WriteRefCountEvent(): to be called when the reference count changes. 
  During runtime, this function collects the stacktrace and attaches it to 
  the event. 


- Rebuild your project

Step 2:
- Run startLog.bat to start the recording session

- Run your program (reproduce the steps which cause the leak)

- Run endLog.bat to end the recording (but leave your program running)
  This will save the log into EventLog\RefCntLog.etl
  
Step 3:
- Run processLog.bat to process the log 
 This will tally the increment/decrement and print out to XML the call 
sequence tree that leak the resource. 

