#ifndef _SymDB_h_
#define _SymDB_h_

#define MAX_SYMBOL_NAME_LENGTH 255

#include <windows.h>
#include <DbgHelp.h>
#include <map>
#include <set>
#include <algorithm>
#include <vector>

struct FuncInfo;
struct ModuleInfo;

//=============================================================================
// Description:
//   This struct represents a call instruction that triggers a refcount change
//=============================================================================
struct CallInstructionInfo
{
	DWORD64 m_Offset;				// offset in memory
	FuncInfo* m_pContainerFuncInfo;	// pointer to the function this instruction is in

	IMAGEHLP_LINE64 m_LineInfo;		// the line of the call in the source code
	DWORD m_LineDisplacement;		// the column of the call in the source code
	
	std::wstring m_FileName;			// the source code file name
	
	DWORD m_AddRefCount;			// number of AddRef (increment) made by this same instruction
	DWORD m_ReleaseCount;			// number of Release (decrement) made by this same instruction

	void AddRef() { m_AddRefCount++;}
	void Release(){ m_ReleaseCount++;}

	DWORD64 GetAddr() const	{ return m_Offset;}

	CallInstructionInfo(DWORD64 in_offset);

	void PrintErrorLog(DWORD in_nIndent, std::wostream& out_str);
	void PrintXML(DWORD in_nIndent, std::wostream& out_str);
};
typedef CallInstructionInfo* CallInstructionInfoPtr;

//=============================================================================
// Description:
//   Represent a function 
//=============================================================================
struct FuncInfo
{
	//=====================================
	// Member variables
	//=====================================

	// structure to get the function name from running process
	// This is passed to SymFromAddr()
	struct sym_pack_tag {
		SYMBOL_INFO			sym;							// info about the (function) symbol
		char				name[MAX_SYMBOL_NAME_LENGTH];	// string containing the function decorated name
	} m_SymPack;

	char m_UndecoratedName[ MAX_SYMBOL_NAME_LENGTH ];		// undecorated name

	IMAGEHLP_MODULE64 m_ModuleInfo;							// info about the module

	typedef std::map< DWORD64, CallInstructionInfoPtr > AddrToCallInstructionMap;
	AddrToCallInstructionMap m_AddrToCallInstructionMap;	// map of the call instructions (to other functions) in this function

	//=====================================
	// Member functions
	//=====================================
	FuncInfo(DWORD64 in_offset, HANDLE in_hTracedProcess);
	~FuncInfo();

	inline DWORD64 GetAddr() const { return m_SymPack.sym.Address;}

	CallInstructionInfoPtr GetCallInstruction(DWORD64 in_offset);
	CallInstructionInfoPtr GetCallInstruction(DWORD64 in_offset, HANDLE in_hTracedProcess);

	void PrintErrorLog(INT32 in_nIndent, std::wostream& out_str);
	void PrintXML(INT32 in_nIndent, std::wostream& out_str);

	DWORD64 GetAddRefCount() const;
	DWORD64 GetReleaseCount() const;
};
typedef FuncInfo* FuncInfoPtr;


//=============================================================================
// Description:
//   Represent a module
//=============================================================================
struct ModuleInfo
{
	IMAGEHLP_MODULE64 m_ModuleInfo;
	DWORD64 m_AddRefCount;
	DWORD64 m_ReleaseCount;
	typedef std::map< DWORD64, FuncInfoPtr > AddrToFuncInfoMap;
	AddrToFuncInfoMap m_AddrToFuncInfoMap;

	ModuleInfo(const IMAGEHLP_MODULE64& in_moduleInfo);

	void AddRef();
	void Release();

	void AddFunc(FuncInfoPtr in_f);

	void PrintErrorLog(INT32 in_nIndent, std::wostream& out_str);
	void PrintXML(INT32 in_nIndent, std::wostream& out_str);

};
typedef ModuleInfo* ModuleInfoPtr;
typedef std::set<ModuleInfoPtr> ModuleInfoPtrSet;
typedef std::map< DWORD64, ModuleInfoPtr > AddrToModuleMap;

struct CallStackItem;
typedef CallStackItem* CallStackItemPtr;

//=============================================================================
// Description:
//   Represent an item in a "function call tree", constructed from a recorded
//   sequence of callstacks
//   An item is a function with its record of call instructions
// Example:
//   item: "myApp::run"
//     item: "A::execute"
//       item: "A::initResource"
//       item: "A::processResource"
//       item: "A::deleteResource"
//     item: "B::execute"
//
//=============================================================================
struct CallStackItem
{
private:
	friend class SymStatsDB;

	typedef std::vector< CallStackItemPtr > CallStackItemPtrVector;


	DWORD m_AddRefCount;			// number of increments made by the subtree
	DWORD m_ReleaseCount;			// number of decrements made by the subtree

	DWORD64  m_Offset;				// ?

	FuncInfoPtr m_pCalleFuncInfo;	// the function being called
	CallInstructionInfoPtr m_pCallerInstructionInfo;	// the call instruction that triggered this item

	CallStackItemPtrVector m_SubCalls; // child items, which are triggered by this m_pCalleeFunc

public:
	CallStackItem();
	CallStackItem(DWORD64 in_offset, FuncInfoPtr in_CalleeFuncInfo, CallInstructionInfoPtr in_pCallerInstructionInfo);
	~CallStackItem();

	void AddRef();
	void Release();

	CallStackItemPtr AddCall( DWORD64 in_offset, bool in_bIsAddRef, FuncInfoPtr in_pCalleeInfo, CallInstructionInfoPtr in_pCallerInstruction );
	void PrintErrorLog(INT32 in_nIndent, std::wostream& out_str, bool bRecursive = false);
	void PrintXML(INT32 in_nIndent, std::wostream& out_str);
};

//=============================================================================
// Description:
//   Database containing statistics about the functions encountered during
//   processing of the event log
//=============================================================================

class SymStatsDB
{
public:
	static SymStatsDB* msInstance;

	static void Init(HANDLE in_hProcess);
	static void Destroy();

	SymStatsDB(HANDLE in_hProcess);

	~SymStatsDB();

	void BeginCallStack(bool in_bIsAddRefEvent);
	void EndCallStack();

	void PushCallStackItem(DWORD64 in_offset, bool in_bIsAddRef);

	void PrintErrorLog(const std::wstring& in_sFilename);
	void PrintXML(const std::wstring& in_sFilename);
	//void SetupFilterModules(const std::string& moduleNames);

	void GetCallInstruction(DWORD64 in_offset, CallInstructionInfoPtr& out_callInstr, ModuleInfoPtr& out_module);

	__inline bool IsModuleInFilter(const IMAGEHLP_MODULE64& moduleInfo) const
	{
		return m_FilterModuleMap.empty() || m_FilterModuleMap.find(moduleInfo.BaseOfImage) != m_FilterModuleMap.end();
	}

private:
	// Process being traced
	HANDLE m_hTracedProcess;

	// cache map to speed up PC Addr -> CallInstructionInfo
	typedef std::map< DWORD64, CallInstructionInfoPtr > AddrToCallInstructionMap;
	AddrToCallInstructionMap m_AddrToCallInstructionMap;

	// map from Function Base Addr -> FuncInfo
	typedef std::map< DWORD64, FuncInfoPtr > AddrToFuncMap;
	AddrToFuncMap m_AddrToFuncMap;

	// map from module baseAddr -> ModuleInfo
	AddrToModuleMap m_AddrToModuleMap;

	// map of modules we want to print the detailed callstack of
	AddrToModuleMap m_FilterModuleMap;

	// modules involved in current callstack, each of them
	// will be addref/release only once for the whole callstack
	ModuleInfoPtrSet m_CurStackModules;

	// call sequence stats
	CallStackItemPtr m_pRootCallItem;
	CallStackItemPtr m_pCurrCallItem;
	DWORD64 m_LastOffset;
	bool m_bCurEventIsAddRef;
};

#endif