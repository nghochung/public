// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "SymDB.h"

//Turns the DEFINE_GUID for EventTraceGuid into a const.
#define INITGUID

#include <stdio.h>
#include <wbemidl.h>
#include <wmistr.h>
#include <evntrace.h>
#include <tdh.h>
#include <in6addr.h>

#include <xsi_instrument.h>

#pragma comment(lib, "tdh.lib")

// Used to calculate CPU usage

ULONG g_TimerResolution = 0;

// Used to determine if the session is a private session or kernel session.
// You need to know this when accessing some members of the EVENT_TRACE.Header
// member (for example, KernelTime or UserTime).

BOOL g_bUserMode = FALSE;

// Handle to the trace file that you opened.

TRACEHANDLE g_hTrace = 0;  

// Prototypes

void WINAPI ProcessEvent(PEVENT_RECORD pEvent);
DWORD GetEventInformation(PEVENT_RECORD pEvent, PTRACE_EVENT_INFO & pInfo);
DWORD GetArraySize(PEVENT_RECORD pEvent, PTRACE_EVENT_INFO pInfo, USHORT i, PUINT32 ArraySize);
DWORD ProcessRefCountEventProperties(PEVENT_RECORD pEvent, PTRACE_EVENT_INFO pInfo, LPWSTR pStructureName, USHORT StructIndex, BOOL IsAddRef);

int _tmain(int argc, _TCHAR* argv[])
{

	TDHSTATUS status = ERROR_SUCCESS;
	EVENT_TRACE_LOGFILE trace;
	TRACE_LOGFILE_HEADER* pHeader = &trace.LogfileHeader;

	// Identify the log file from which you want to consume events
	// and the callbacks used to process the events and buffers.

	// arg[1] is event log file
	ZeroMemory(&trace, sizeof(EVENT_TRACE_LOGFILE));
	trace.LogFileName = (LPWSTR) argv[1];
	trace.EventRecordCallback = (PEVENT_RECORD_CALLBACK) (ProcessEvent);
	trace.ProcessTraceMode = PROCESS_TRACE_MODE_EVENT_RECORD;

	// arg[2] is manifest file
	LPWSTR manifestFilepath = (argc > 2) ? (LPWSTR)argv[2] : NULL;

	// arg[3] is output formal (xml/log)
	bool bXml = (argc>3 && argv[3] && (_tcscmp( argv[3], L"xml" ) == 0));
	
	// arg[4] is output file
	std::wstring outputFilename;
	if (argc > 4)
	{
		outputFilename = (LPWSTR)argv[4];
	}
	else
	{
		outputFilename = bXml ? L"output.xml" : L"output.log";
	}

	if (manifestFilepath == NULL || TdhLoadManifest(manifestFilepath) != ERROR_SUCCESS)
	{
		wprintf(L"Manifest not found\n", GetLastError());
		goto cleanup;
	}

	g_hTrace = OpenTrace(&trace);
	if (INVALID_PROCESSTRACE_HANDLE == g_hTrace)
	{
		wprintf(L"OpenTrace failed with %lu\n", GetLastError());
		goto cleanup;
	}

	g_bUserMode = pHeader->LogFileMode & EVENT_TRACE_PRIVATE_LOGGER_MODE;

	if (pHeader->TimerResolution > 0)
	{
		g_TimerResolution = pHeader->TimerResolution / 10000;
	}

	wprintf(L"Number of events lost:  %lu\n", pHeader->EventsLost);

	// Use pHeader to access all fields prior to LoggerName.
	// Adjust pHeader based on the pointer size to access
	// all fields after LogFileName. This is required only if
	// you are consuming events on an architecture that is 
	// different from architecture used to write the events.

	if (pHeader->PointerSize != sizeof(PVOID))
	{
		pHeader = (PTRACE_LOGFILE_HEADER)((PUCHAR)pHeader +
			2 * (pHeader->PointerSize - sizeof(PVOID)));
	}

	wprintf(L"Number of buffers lost: %lu\n\n", pHeader->BuffersLost);

	status = ProcessTrace(&g_hTrace, 1, 0, 0);
	if (status != ERROR_SUCCESS && status != ERROR_CANCELLED)
	{
		wprintf(L"ProcessTrace failed with %lu\n", status);
		goto cleanup;
	}

	if (bXml)
		SymStatsDB::msInstance->PrintXML(outputFilename);
	else
		SymStatsDB::msInstance->PrintErrorLog(outputFilename);

cleanup:

	if (INVALID_PROCESSTRACE_HANDLE != g_hTrace)
	{
		status = CloseTrace(g_hTrace);
	}

	SymStatsDB::Destroy();
}


// Callback that receives the events. 

VOID WINAPI ProcessEvent(PEVENT_RECORD pEvent)
{
	HANDLE pTracedProcess = NULL;

	DWORD status = ERROR_SUCCESS;
	PTRACE_EVENT_INFO pInfo = NULL;
	LPWSTR pwsEventGuid = NULL;
	PBYTE pUserData = NULL;
	PBYTE pEndOfUserData = NULL;
	DWORD PointerSize = 0;
	ULONGLONG TimeStamp = 0;
	ULONGLONG Nanoseconds = 0;
	SYSTEMTIME st;
	SYSTEMTIME stLocal;
	FILETIME ft;


	// Skips the event if it is the event trace header. Log files contain this event
	// but real-time sessions do not. The event contains the same information as 
	// the EVENT_TRACE_LOGFILE.LogfileHeader member that you can access when you open 
	// the trace. 

	if (IsEqualGUID(pEvent->EventHeader.ProviderId, EventTraceGuid) &&
		pEvent->EventHeader.EventDescriptor.Opcode == EVENT_TRACE_TYPE_INFO)
	{
		; // Skip this event.
	}
	else if ( IsEqualGUID(pEvent->EventHeader.ProviderId, xsiProviderGuid) )
	{
		if ( SymStatsDB::msInstance == NULL )
		{
			pTracedProcess = OpenProcess( PROCESS_ALL_ACCESS, TRUE, pEvent->EventHeader.ProcessId );

			if ( pTracedProcess == NULL )
			{
				wprintf(L"Process not found: %d\n", pEvent->EventHeader.ProcessId);
				goto cleanup;
			}

			if ( SymInitialize( pTracedProcess, NULL, TRUE ) == FALSE )
			{
				wprintf( L"SymInitialize failed, last error %x\n", GetLastError() );
				goto cleanup;
			}

			SymStatsDB::Init( pTracedProcess );
		}

		// Process the event. The pEvent->UserData member is a pointer to 
		// the event specific data, if it exists.

		status = GetEventInformation(pEvent, pInfo);

		if (ERROR_SUCCESS != status)
		{
			wprintf(L"GetEventInformation failed with %lu\n", status);
			goto cleanup;
		}

		// Determine whether the event is defined by a MOF class, in an
		// instrumentation manifest, or a WPP template; to use TDH to decode
		// the event, it must be defined by one of these three sources.

		if (DecodingSourceWbem == pInfo->DecodingSource)  // MOF class
		{
			HRESULT hr = StringFromCLSID(pInfo->EventGuid, &pwsEventGuid);

			if (FAILED(hr))
			{
				wprintf(L"StringFromCLSID failed with 0x%x\n", hr);
				status = hr;
				goto cleanup;
			}

			wprintf(L"\nEvent GUID: %s\n", pwsEventGuid);
			CoTaskMemFree(pwsEventGuid);
			pwsEventGuid = NULL;

			wprintf(L"Event version: %d\n", pEvent->EventHeader.EventDescriptor.Version);
			wprintf(L"Event type: %d\n", pEvent->EventHeader.EventDescriptor.Opcode);
		}
		else if (DecodingSourceXMLFile == pInfo->DecodingSource) // Instrumentation manifest
		{
			//wprintf(L"Event ID: %d\n", pInfo->EventDescriptor.Id);
		}
		else // Not handling the WPP case
		{
			goto cleanup;
		}

		// Print the time stamp for when the event occurred.

		ft.dwHighDateTime = pEvent->EventHeader.TimeStamp.HighPart;
		ft.dwLowDateTime = pEvent->EventHeader.TimeStamp.LowPart;

		FileTimeToSystemTime(&ft, &st);
		SystemTimeToTzSpecificLocalTime(NULL, &st, &stLocal);

		TimeStamp = pEvent->EventHeader.TimeStamp.QuadPart;
		Nanoseconds = (TimeStamp % 10000000) * 100;

		//wprintf(L"%02d/%02d/%02d %02d:%02d:%02d.%I64u\n", stLocal.wMonth, stLocal.wDay, stLocal.wYear, stLocal.wHour, stLocal.wMinute, stLocal.wSecond, Nanoseconds);

		// If the event contains event-specific data use TDH to extract
		// the event data. For this example, to extract the data, the event 
		// must be defined by a MOF class or an instrumentation manifest.

		// Need to get the PointerSize for each event to cover the case where you are
		// consuming events from multiple log files that could have been generated on 
		// different architectures. Otherwise, you could have accessed the pointer
		// size when you opened the trace above (see pHeader->PointerSize).

		if (EVENT_HEADER_FLAG_32_BIT_HEADER == (pEvent->EventHeader.Flags & EVENT_HEADER_FLAG_32_BIT_HEADER))
		{
			PointerSize = 4;
		}
		else
		{
			PointerSize = 8;
		}

		pUserData = (PBYTE)pEvent->UserData;
		pEndOfUserData = (PBYTE)pEvent->UserData + pEvent->UserDataLength;

		// Print the event data for all the top-level properties. Metadata for all the 
		// top-level properties come before structure member properties in the 
		// property information array.

		switch (pInfo->EventDescriptor.Id)
		{
		case AddRefEvent_value: ProcessRefCountEventProperties(pEvent, pInfo, NULL, 0, TRUE); break;
		case DecRefEvent_value: ProcessRefCountEventProperties(pEvent, pInfo, NULL, 0, FALSE); break;
		}

	}

cleanup:

	if (pInfo)
	{
		free(pInfo);
	}

	if (ERROR_SUCCESS != status || NULL == pUserData)
	{
		CloseTrace(g_hTrace);
	}

	if ( pTracedProcess != NULL )
	{
		CloseHandle( pTracedProcess );
	}
}


// Print the property.
DWORD ProcessRefCountEventProperties(PEVENT_RECORD pEvent, PTRACE_EVENT_INFO pInfo, LPWSTR pStructureName, USHORT StructIndex, BOOL IsAddRef)
{
	DWORD status = ERROR_SUCCESS;
	DWORD LastMember = 0;  // Last member of a structure
	
	PEVENT_MAP_INFO pMapInfo = NULL;
	PROPERTY_DATA_DESCRIPTOR DataDescriptors[2];
	ULONG DescriptorsCount = 0;
	DWORD PropertySize = 0;
	PBYTE pData = NULL;

	const ULONG PROPERTY_INDEX_ADDR = 0;
	const ULONG PROPERTY_INDEX_STACKDEPTH = 1;
	const ULONG PROPERTY_INDEX_STACKTRACE = 2;

	// Get 1st property: address of instance
	if (pStructureName)
	{
		DataDescriptors[0].PropertyName = (ULONGLONG)pStructureName;
		DataDescriptors[0].ArrayIndex = StructIndex;
		DataDescriptors[1].PropertyName = (ULONGLONG)((PBYTE)(pInfo) + pInfo->EventPropertyInfoArray[PROPERTY_INDEX_ADDR].NameOffset);
		DataDescriptors[1].ArrayIndex = 0;
		DescriptorsCount = 2;
	}
	else
	{
		DataDescriptors[0].PropertyName = (ULONGLONG)((PBYTE)(pInfo) + pInfo->EventPropertyInfoArray[PROPERTY_INDEX_ADDR].NameOffset);
		DataDescriptors[0].ArrayIndex = 0;
		DescriptorsCount = 1;
	}
	
	UINT64 instanceAddr;
	status = TdhGetProperty(pEvent, 0, NULL, DescriptorsCount, &DataDescriptors[0], sizeof(instanceAddr), (PBYTE)&instanceAddr );

	// Get 2nd and 3rd property: stack depth and stack trace
	UINT32 stackDepth;

	status = GetArraySize(pEvent, pInfo, 2, &stackDepth);

	SymStatsDB::msInstance->BeginCallStack(IsAddRef==TRUE);

	for (INT32 k = stackDepth - 1; k >= 0; k--)
	{
		//wprintf(L"%*s%s: ", (pStructureName) ? 4 : 0, L"", (LPWSTR)((PBYTE)(pInfo) + pInfo->EventPropertyInfoArray[PROPERTY_INDEX_STACKTRACE].NameOffset));

		// If the property is a structure, print the members of the structure.

		ZeroMemory(&DataDescriptors, sizeof(DataDescriptors));

		// To retrieve a member of a structure, you need to specify an array of descriptors. 
		// The first descriptor in the array identifies the name of the structure and the second 
		// descriptor defines the member of the structure whose data you want to retrieve. 

		if (pStructureName)
		{
			DataDescriptors[0].PropertyName = (ULONGLONG)pStructureName;
			DataDescriptors[0].ArrayIndex = StructIndex;
			DataDescriptors[1].PropertyName = (ULONGLONG)((PBYTE)(pInfo) + pInfo->EventPropertyInfoArray[PROPERTY_INDEX_STACKTRACE].NameOffset);
			DataDescriptors[1].ArrayIndex = k;
			DescriptorsCount = 2;
		}
		else
		{
			DataDescriptors[0].PropertyName = (ULONGLONG)((PBYTE)(pInfo) + pInfo->EventPropertyInfoArray[PROPERTY_INDEX_STACKTRACE].NameOffset);
			DataDescriptors[0].ArrayIndex = k;
			DescriptorsCount = 1;
		}

		// The TDH API does not support IPv6 addresses. If the output type is TDH_OUTTYPE_IPV6,
		// you will not be able to consume the rest of the event. If you try to consume the
		// remainder of the event, you will get ERROR_EVT_INVALID_EVENT_DATA.

		DWORD64 PCAddr;

		if (TDH_INTYPE_UINT64 == pInfo->EventPropertyInfoArray[PROPERTY_INDEX_STACKTRACE].nonStructType.InType)
		{
			status = TdhGetPropertySize(pEvent, 0, NULL, DescriptorsCount, &DataDescriptors[0], &PropertySize);

			if (ERROR_SUCCESS != status)
			{
				wprintf(L"TdhGetPropertySize failed with %lu\n", status);
				goto cleanup;
			}

			if ( PropertySize != sizeof(DWORD64) )
			{
				wprintf(L"Stacktrace item size not valid.\n");
				status = ERROR_INVALID_DATA;
				goto cleanup;
			}

			status = TdhGetProperty(pEvent, 0, NULL, DescriptorsCount, &DataDescriptors[0], PropertySize, (PBYTE)&PCAddr );

			SymStatsDB::msInstance->PushCallStackItem( PCAddr, IsAddRef==TRUE );
		}
	}

	SymStatsDB::msInstance->EndCallStack();

cleanup:

	return status;
}

// Get the size of the array. For MOF-based events, the size is specified in the declaration or using 
// the MAX qualifier. For manifest-based events, the property can specify the size of the array
// using the count attribute. The count attribue can specify the size directly or specify the name 
// of another property in the event data that contains the size.

DWORD GetArraySize(PEVENT_RECORD pEvent, PTRACE_EVENT_INFO pInfo, USHORT i, PUINT32 ArraySize)
{
	DWORD status = ERROR_SUCCESS;
	PROPERTY_DATA_DESCRIPTOR DataDescriptor;
	DWORD PropertySize = 0;

	if ((pInfo->EventPropertyInfoArray[i].Flags & PropertyParamCount) == PropertyParamCount)
	{
		DWORD Count = 0;  // Expects the count to be defined by a UINT16 or UINT32
		DWORD j = pInfo->EventPropertyInfoArray[i].countPropertyIndex;
		ZeroMemory(&DataDescriptor, sizeof(PROPERTY_DATA_DESCRIPTOR));
		DataDescriptor.PropertyName = (ULONGLONG)((PBYTE)(pInfo) + pInfo->EventPropertyInfoArray[j].NameOffset);
		DataDescriptor.ArrayIndex = ULONG_MAX;
		status = TdhGetPropertySize(pEvent, 0, NULL, 1, &DataDescriptor, &PropertySize);
		status = TdhGetProperty(pEvent, 0, NULL, 1, &DataDescriptor, PropertySize, (PBYTE)&Count);
		*ArraySize = (UINT32)Count;
	}
	else
	{
		*ArraySize = pInfo->EventPropertyInfoArray[i].count;
	}

	return status;
}


// Get the metadata for the event.

DWORD GetEventInformation(PEVENT_RECORD pEvent, PTRACE_EVENT_INFO & pInfo)
{
	DWORD status = ERROR_SUCCESS;
	DWORD BufferSize = 0;

	// Retrieve the required buffer size for the event metadata.

	status = TdhGetEventInformation(pEvent, 0, NULL, pInfo, &BufferSize);

	if (ERROR_INSUFFICIENT_BUFFER == status)
	{
		pInfo = (TRACE_EVENT_INFO*) malloc(BufferSize);
		if (pInfo == NULL)
		{
			wprintf(L"Failed to allocate memory for event info (size=%lu).\n", BufferSize);
			status = ERROR_OUTOFMEMORY;
			goto cleanup;
		}

		// Retrieve the event metadata.

		status = TdhGetEventInformation(pEvent, 0, NULL, pInfo, &BufferSize);
	}

	if (ERROR_SUCCESS != status)
	{
		wprintf(L"TdhGetEventInformation failed with 0x%x.\n", status);
	}

cleanup:

	return status;
}
