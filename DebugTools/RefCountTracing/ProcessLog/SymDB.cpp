#include "SymDB.h"
#include <fstream>
#include <iostream>
#include <memory>    // For std::unique_ptr
#include <stdarg.h>  // For va_start, etc.
#include <string>
#include <tchar.h>
#include <boost/format.hpp>
#include <boost/multiprecision/cpp_int.hpp>

using namespace boost::multiprecision;

//=============================================================================
// CallInstructionInfo
//=============================================================================
CallInstructionInfo::CallInstructionInfo(DWORD64 in_offset)
{
	m_Offset = in_offset;
	memset(&m_LineInfo, 0, sizeof(m_LineInfo));
	m_pContainerFuncInfo = nullptr;
	m_AddRefCount = m_ReleaseCount = 0;
}

void CallInstructionInfo::PrintErrorLog(DWORD in_nIndent, std::wostream& out_str)
{
	const std::wstring sTab(in_nIndent, _T('\t'));
	out_str << sTab;
	out_str << boost::wformat(_T("%s(%d,%d): error REFCNT0: addref=%d, release=%d, diff=%d\n")) 
				% m_FileName % m_LineInfo.LineNumber % m_LineDisplacement % m_AddRefCount % m_ReleaseCount % (m_AddRefCount - m_ReleaseCount);
}

void CallInstructionInfo::PrintXML(DWORD in_nIndent, std::wostream& out_str)
{
	const std::wstring sTab(in_nIndent, _T('\t'));

	out_str << sTab;
	out_str << boost::wformat(_T("<CallInstruction file=\"%s\" line=\"%d\"  addref=\"%d\" release=\"%d\"")) 
				% m_FileName % m_LineInfo.LineNumber % m_AddRefCount % m_ReleaseCount;
	
	if (m_AddRefCount != m_ReleaseCount)
		out_str << boost::wformat(_T(" diff=\"%d\"")) % (m_AddRefCount - m_ReleaseCount);

	out_str << "/>\n";
}

//=============================================================================
// FuncInfo
//=============================================================================
FuncInfo::FuncInfo(DWORD64 in_offset, HANDLE in_hTracedProcess)
{
	memset(&m_SymPack, 0, sizeof(m_SymPack));
	memset(&m_UndecoratedName, 0, sizeof(m_UndecoratedName));
	memset(&m_ModuleInfo, 0, sizeof(m_ModuleInfo));

	m_SymPack.sym.SizeOfStruct = sizeof(SYMBOL_INFO);
	m_SymPack.sym.MaxNameLen = MAX_SYMBOL_NAME_LENGTH;
	m_ModuleInfo.SizeOfStruct = sizeof(IMAGEHLP_MODULE64);

	PSYMBOL_INFO pSymbol = &m_SymPack.sym;

	DWORD64 displacement = 0;

	if (SymGetModuleInfo64(in_hTracedProcess, (ULONG64)in_offset, &m_ModuleInfo) == TRUE)
	{

	}
	else
	{
		wprintf(L"SymGetModuleInfo64 failed, error code %x\n", GetLastError());
	}

	if (SymFromAddr(in_hTracedProcess, (ULONG64)in_offset, &displacement, pSymbol) == TRUE)
	{
		UnDecorateSymbolName(pSymbol->Name, (PSTR)m_UndecoratedName, MAX_SYMBOL_NAME_LENGTH, UNDNAME_COMPLETE);
	}
	else
	{
		wprintf(L"SymGetSymFromAddr64 failed, error code %x\n", GetLastError());
		m_SymPack.sym.Address = in_offset;
	}

}

FuncInfo::~FuncInfo()
{
	for (auto& it : m_AddrToCallInstructionMap)
	{
		delete it.second;
	}

	m_AddrToCallInstructionMap.clear();
}

CallInstructionInfoPtr FuncInfo::GetCallInstruction(DWORD64 in_offset)
{
	AddrToCallInstructionMap::iterator it = m_AddrToCallInstructionMap.find(in_offset);

	if (it != m_AddrToCallInstructionMap.end())
	{
		return it->second;
	}

	return nullptr;
}

CallInstructionInfoPtr FuncInfo::GetCallInstruction(DWORD64 in_offset, HANDLE in_hTracedProcess)
{
	AddrToCallInstructionMap::iterator it = m_AddrToCallInstructionMap.find(in_offset);

	if (it != m_AddrToCallInstructionMap.end())
	{
		return it->second;
	}
	else
	{
		CallInstructionInfoPtr p = new CallInstructionInfo(in_offset);

		p->m_pContainerFuncInfo = this;

		DWORD lineDisp;
		IMAGEHLP_LINE64 lineInfo;

		if (SymGetLineFromAddr64(in_hTracedProcess, (ULONG64)in_offset, &lineDisp, &lineInfo) == TRUE)
		{
			p->m_LineDisplacement = lineDisp;
			p->m_LineInfo = lineInfo;
			std::string sFilename = lineInfo.FileName;
			p->m_FileName = std::wstring(sFilename.begin(), sFilename.end());
		}

		m_AddrToCallInstructionMap[in_offset] = p;

		return p;
	}
}


void FuncInfo::PrintErrorLog(INT32 in_nIndent, std::wostream& out_str)
{
	DWORD64 addRefCount = GetAddRefCount(), releaseCount = GetReleaseCount();

	if (addRefCount != releaseCount)
	{
		const std::wstring sTab(in_nIndent, _T('\t'));
		out_str << sTab;
		out_str << boost::wformat(_T("%s: addref=%d release=%d diff=%d\n"))
					% m_UndecoratedName % addRefCount % releaseCount % (addRefCount - releaseCount);

		for (const auto& it : m_AddrToCallInstructionMap)
		{
			if (it.second->m_AddRefCount != it.second->m_ReleaseCount)
				it.second->PrintErrorLog(in_nIndent + 1, out_str);
		}
	}
}

void FuncInfo::PrintXML(INT32 in_nIndent, std::wostream& out_str)
{
	const std::wstring sTab(in_nIndent, _T('\t'));

	DWORD64 addRefCount = GetAddRefCount(), releaseCount = GetReleaseCount();

	if (addRefCount != releaseCount)
	{
		out_str << sTab;
		out_str << boost::wformat(_T("<Function addr=\"0x%X\" module=\"%s\" name=\"%s\" addref=\"%d\" release=\"%d\""))
					% m_SymPack.sym.Address % m_ModuleInfo.ModuleName % m_UndecoratedName % addRefCount % releaseCount;
		
		out_str << boost::wformat(_T(" diff=\"%d\">\n")) % int64_t(addRefCount - releaseCount);

		for (const auto& it : m_AddrToCallInstructionMap)
		{
			if (it.second->m_AddRefCount != it.second->m_ReleaseCount)
				it.second->PrintXML(in_nIndent + 1, out_str);
		}

		out_str << sTab;
		out_str << _T("</Function>\n");
	}
}

DWORD64 FuncInfo::GetAddRefCount() const
{
	DWORD64 addRefCount = 0;
	for (const auto& it : m_AddrToCallInstructionMap)
	{
		addRefCount += it.second->m_AddRefCount;
	}

	return addRefCount;
}

DWORD64 FuncInfo::GetReleaseCount() const
{
	DWORD64 releaseCount = 0;
	for (const auto& it : m_AddrToCallInstructionMap)
	{
		releaseCount += it.second->m_ReleaseCount;
	}

	return releaseCount;
}

//=============================================================================
// ModuleInfo
//=============================================================================
ModuleInfo::ModuleInfo(const IMAGEHLP_MODULE64& in_moduleInfo)
{
	memcpy((void*)&m_ModuleInfo, (const void*)&in_moduleInfo, sizeof(in_moduleInfo));
	m_AddRefCount = m_ReleaseCount = 0;
}

void ModuleInfo::AddRef()
{
	m_AddRefCount++;
}

void ModuleInfo::Release()
{
	m_ReleaseCount++;
}

void ModuleInfo::AddFunc(FuncInfoPtr in_f)
{
	m_AddrToFuncInfoMap[in_f->GetAddr()] = in_f;
}

void ModuleInfo::PrintErrorLog(INT32 in_nIndent, std::wostream& out_str)
{
	if (m_AddRefCount != m_ReleaseCount)
	{
		typedef std::vector<FuncInfoPtr> FuncInfoPtrVector;
		FuncInfoPtrVector sortedFuncs;

		for (const auto& it : m_AddrToFuncInfoMap)
		{
			DWORD64 funcAddRefCount = it.second->GetAddRefCount();
			DWORD64 funcReleaseCount = it.second->GetReleaseCount();

			if (funcAddRefCount != funcReleaseCount)
				sortedFuncs.push_back(it.second);
		}

		std::sort(sortedFuncs.begin(), sortedFuncs.end(), [](const FuncInfoPtr f1, const FuncInfoPtr f2){
				return strcmp(f1->m_UndecoratedName, f2->m_UndecoratedName) < 0;
			});

		out_str << boost::wformat(_T("Module %s: addref=%d release=%d diff=%d\n"))
					% m_ModuleInfo.ModuleName % m_AddRefCount % m_ReleaseCount % (m_AddRefCount - m_ReleaseCount);

		for (const auto& it : sortedFuncs)
		{
			it->PrintErrorLog(in_nIndent + 1, out_str);
		}
	}
}

void ModuleInfo::PrintXML(INT32 in_nIndent, std::wostream& out_str)
{
	if (m_AddRefCount != m_ReleaseCount)
	{
		typedef std::vector<FuncInfoPtr> FuncInfoPtrVector;
		FuncInfoPtrVector sortedFuncs;

		for (const auto& it : m_AddrToFuncInfoMap)
		{
			DWORD64 funcAddRefCount = it.second->GetAddRefCount();
			DWORD64 funcReleaseCount = it.second->GetReleaseCount();

			if (funcAddRefCount != funcReleaseCount)
				sortedFuncs.push_back(it.second);
		}

		std::sort(sortedFuncs.begin(), sortedFuncs.end(), [](const FuncInfoPtr f1, const FuncInfoPtr f2){
			return strcmp(f1->m_UndecoratedName, f2->m_UndecoratedName) < 0;
		});

		int32_t add = m_AddRefCount;
		int32_t rel = m_ReleaseCount;

		out_str << boost::wformat(_T("<Module name=\"%s\" addref=\"%d\" release=\"%d\" diff=\"%lld\">\n"))
			% m_ModuleInfo.ModuleName % m_AddRefCount % m_ReleaseCount % int32_t(add - rel);
		
		for (const auto& it : sortedFuncs)
		{
			it->PrintXML(1, out_str);
		}

		out_str << _T("</Module>\n");

	}
}

//=============================================================================
// ModuleInfo
//=============================================================================

CallStackItem::CallStackItem()
{
	m_pCalleFuncInfo = nullptr;
	m_pCallerInstructionInfo = nullptr;
	m_AddRefCount = m_ReleaseCount = 0;
}

CallStackItem::CallStackItem(DWORD64 in_offset, FuncInfoPtr in_CalleeFuncInfo, CallInstructionInfoPtr in_pCallerInstructionInfo)
{
	m_Offset = in_offset;
	m_pCalleFuncInfo = in_CalleeFuncInfo;
	m_pCallerInstructionInfo = in_pCallerInstructionInfo;
	m_AddRefCount = m_ReleaseCount = 0;
}

CallStackItem::~CallStackItem()
{
	for (auto pSubItem : m_SubCalls)
	{
		delete pSubItem;
	}

	m_SubCalls.clear();
}

void CallStackItem::AddRef()
{
	m_AddRefCount++;
}

void CallStackItem::Release()
{
	m_ReleaseCount++;
}

CallStackItemPtr CallStackItem::AddCall(DWORD64 in_PCaddr, bool in_bIsAddRef, FuncInfoPtr in_pCalleeInfo, CallInstructionInfoPtr in_pCallerInstruction)
{
	if (in_bIsAddRef)
		AddRef();
	else
		Release();

	if (!m_SubCalls.empty())
	{
		CallStackItemPtr lastItem = m_SubCalls.back();
		if (lastItem->m_Offset == in_PCaddr)
		{
			return lastItem;
		}

	}

	CallStackItemPtr pItem = new CallStackItem(in_PCaddr, in_pCalleeInfo, in_pCallerInstruction);
	this->m_SubCalls.push_back(pItem);
	return pItem;
}

void CallStackItem::PrintXML(INT32 in_nIndent, std::wostream& out_str)
{
	if (m_AddRefCount != m_ReleaseCount)
	{
		const std::wstring sTab(in_nIndent, _T('\t'));

		out_str << sTab;
		out_str << boost::wformat(_T("<Call func=\"%s\" addref=\"%d\" release=\"%d\""))
					% m_pCalleFuncInfo->m_UndecoratedName % m_AddRefCount % m_ReleaseCount;

		out_str << boost::wformat(_T(" diff=\"%d\"")) % (m_AddRefCount - m_ReleaseCount);

		out_str << _T(">\n");

		for (const auto& pSubItem : m_SubCalls)
			pSubItem->PrintXML(in_nIndent + 1, out_str);

		out_str << sTab;
		out_str << _T("</Call>\n");
	}
}

//=============================================================================
// SymStatsDB
//=============================================================================
SymStatsDB* SymStatsDB::msInstance = nullptr;

void SymStatsDB::Init(HANDLE in_hProcess)
{
	if (!msInstance)
		msInstance = new SymStatsDB(in_hProcess);
}

void SymStatsDB::Destroy()
{
	if (msInstance)
		delete msInstance;
}

SymStatsDB::SymStatsDB(HANDLE in_hProcess) : m_hTracedProcess(in_hProcess)
{
	m_pRootCallItem = new CallStackItem();
}

SymStatsDB::~SymStatsDB()
{
	delete m_pRootCallItem;
	m_pRootCallItem = nullptr;

	for (auto& it : m_AddrToFuncMap)
	{
		delete it.second;
	}
	m_AddrToFuncMap.clear();
	m_AddrToCallInstructionMap.clear();

	for (auto& it : m_AddrToModuleMap)
	{
		delete it.second;
	}

	if (SymCleanup(m_hTracedProcess))
	{
		// SymCleanup returned success
	}
	else
	{
		// SymCleanup failed
		DWORD error = GetLastError();
		printf("SymCleanup returned error : %d\n", error);
	}

}

void SymStatsDB::BeginCallStack(bool in_bIsAddRefEvent)
{
	m_pCurrCallItem = m_pRootCallItem;
	m_bCurEventIsAddRef = in_bIsAddRefEvent;
	m_CurStackModules.clear();
}

void SymStatsDB::EndCallStack()
{
	m_pCurrCallItem = nullptr;
	if (m_bCurEventIsAddRef)
	{
		for (auto pModule : m_CurStackModules)
			pModule->AddRef();
	}
	else
	{
		for (auto pModule : m_CurStackModules)
			pModule->Release();
	}
}

void SymStatsDB::PushCallStackItem(DWORD64 in_PCaddr, bool in_bIsAddRef)
{
	CallInstructionInfoPtr pCallInstr = nullptr;
	ModuleInfoPtr pModule = nullptr;

	GetCallInstruction(in_PCaddr, pCallInstr, pModule);

	if (in_bIsAddRef)
		pCallInstr->AddRef();
	else
		pCallInstr->Release();

	if (pModule)
	{
		m_CurStackModules.insert(pModule);
	}

	m_pCurrCallItem->m_pCalleFuncInfo = pCallInstr->m_pContainerFuncInfo;

	m_pCurrCallItem = m_pCurrCallItem->AddCall(in_PCaddr, in_bIsAddRef, nullptr, pCallInstr);

	m_LastOffset = in_PCaddr;
}

// Description:
//   Add a call instruction to the database
//   in_PCaddr		: address of the instruction (i.e. value in PC register)
//   out_callInstr	: the created CallInstruction
//   out_module		: the ModuleInfo
void SymStatsDB::GetCallInstruction(DWORD64 in_PCaddr, CallInstructionInfoPtr& out_callInstr, ModuleInfoPtr& out_module)
{
	CallInstructionInfoPtr pCallInstr = nullptr;
	ModuleInfoPtr pModule = nullptr;

	AddrToModuleMap::iterator itModule = m_AddrToModuleMap.end();
	AddrToCallInstructionMap::iterator itCallInstr = m_AddrToCallInstructionMap.find(in_PCaddr);

	if (itCallInstr != m_AddrToCallInstructionMap.end())
	{
		// the instruction has been added to the database before, just retrieve it
		pCallInstr = itCallInstr->second;
		itModule = m_AddrToModuleMap.find(itCallInstr->second->m_pContainerFuncInfo->m_ModuleInfo.BaseOfImage);
		if (itModule != m_AddrToModuleMap.end())
			pModule = itModule->second;
	}
	else
	{
		// the instruction is new, figure out the function and the module it is in
		// and add them to the database too
		DWORD64 displacement = 0;
		SYMBOL_INFO sym;
		memset(&sym, 0, sizeof(sym));

		sym.SizeOfStruct = sizeof(SYMBOL_INFO);
		sym.MaxNameLen = 0;

		IMAGEHLP_MODULE64 moduleInfo;
		moduleInfo.SizeOfStruct = sizeof(IMAGEHLP_MODULE64);

		// find the module info
		if (SymGetModuleInfo64(m_hTracedProcess, (ULONG64)in_PCaddr, &moduleInfo) == TRUE)
		{
			// the module info is found
			itModule = m_AddrToModuleMap.find(moduleInfo.BaseOfImage);

			if (itModule != m_AddrToModuleMap.end())
			{
				pModule = itModule->second;
			}
			else
			{
				pModule = new ModuleInfo(moduleInfo);
				m_AddrToModuleMap[moduleInfo.BaseOfImage] = pModule;
			}
		}
		else
		{
			wprintf(L"SymGetModuleInfo64 failed, error code %x\n", GetLastError());
		}


		FuncInfoPtr pFuncInfo = nullptr;

		// find the function info
		if (SymFromAddr(m_hTracedProcess, (ULONG64)in_PCaddr, &displacement, &sym) == TRUE)
		{
			// the function info is found
			AddrToFuncMap::iterator itSym = m_AddrToFuncMap.find(sym.Address);

			if (itSym != m_AddrToFuncMap.end())
			{
				pFuncInfo = itSym->second;
			}
			else
			{
				pFuncInfo = new FuncInfo(in_PCaddr, m_hTracedProcess);

				// Add the symbol (function) to symbol map
				m_AddrToFuncMap[pFuncInfo->GetAddr()] = pFuncInfo;
			}

		}
		else
		{
			// the function info cannot be found probably because the debug
			// info (.pdb file) is not available 
			// We treat the call instruction as a function of its own

			wprintf(L"SymGetSymFromAddr64 failed, error code %x\n", GetLastError());


			AddrToFuncMap::iterator itSym = m_AddrToFuncMap.find(in_PCaddr);

			if (itSym != m_AddrToFuncMap.end())
			{
				pFuncInfo = itSym->second;
			}
			else
			{
				pFuncInfo = new FuncInfo(in_PCaddr, m_hTracedProcess);

				// Add the symbol (function) to symbol map
				m_AddrToFuncMap[pFuncInfo->GetAddr()] = pFuncInfo;
			}

		}

		pCallInstr = pFuncInfo->GetCallInstruction(in_PCaddr, m_hTracedProcess);

		// add the SymDisplacement to cache map
		m_AddrToCallInstructionMap[in_PCaddr] = pCallInstr;

		if (pModule)
			pModule->AddFunc(pFuncInfo);

	}

	out_module = pModule;
	out_callInstr = pCallInstr;
}

void SymStatsDB::PrintXML(const std::wstring& in_sFilename)
{
	std::wofstream out_str(in_sFilename, std::ofstream::out);

	out_str << "<SymStatsDB>\n";
	out_str << "<Modules>\n";
	for (const auto& it : m_AddrToModuleMap)
	{
		it.second->PrintXML(1, out_str);
	}
	out_str << "</Modules>\n";

	// print call tree
	{
		CallStackItemPtr firstBranchingCall = m_pRootCallItem;
		while (firstBranchingCall && firstBranchingCall->m_SubCalls.size() == 1)
		{
			firstBranchingCall = *(firstBranchingCall->m_SubCalls.begin());
		}

		if (firstBranchingCall)
		{
			out_str << "<CallSequences>\n";
			firstBranchingCall->PrintXML(1, out_str);
			out_str << "</CallSequences>\n";
		}
	}


	out_str << "</SymStatsDB>\n";
}

void SymStatsDB::PrintErrorLog(const std::wstring& in_sFilename)
{

}

