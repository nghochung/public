from winappdbg import Debug, EventHandler, System
from winappdbg.win32 import *
from threading import Lock

class MemUser:
    allocTotal = 0
    freeTotal = 0
    lock = None
    name = ""

    def __init__(self, name):
        self.lock = Lock()
        self.name = name

    def alloc(self, size):
        self.lock.acquire()
        try:
            self.allocTotal = self.allocTotal + size
        finally:
            self.lock.release()

    def free(self, size):
        self.lock.acquire()
        try:
            self.freeTotal = self.freeTotal + size
        finally:
            self.lock.release()

class MyEventHandler( EventHandler ):
    users = {}
    lock = None

    # Here we set which API calls we want to intercept.
    apiHooks = {
        # Hooks for the kernel32 library.
        'kernel32.dll' : [
            #  Function            Parameters
            ( 'HeapAlloc'    , (HANDLE, DWORD, SIZE_T) ),
            ( 'HeapFree'     , (HANDLE, DWORD, LPVOID) )
        ]
    }

    def __init__(self):
        super( MyEventHandler, self ).__init__()
        self.users = dict()
        self.lock = Lock()

    # Now we can simply define a method for each hooked API.
    # Methods beginning with "pre_" are called when entering the API,
    # and methods beginning with "post_" when returning from the API.


    def pre_HeapAlloc( self, event, ra, hHeap, dwFlags, szSize ):
        self.__register_mem_event(event, True)

    def pre_HeapFree( self, event, ra, hHeap, dwFlags, lpMem ):
        self.__register_mem_event(event, False)

    def __register_mem_event(self, event, isAlloc):
        # record the functions in the callstack
        System.request_debug_privileges()

        thread = event.get_thread()
        process = event.get_process()

        #process.suspend()
        thread.suspend()

        try:
            stackTrace = thread.get_stack_trace_with_labels()
            for (_, label) in stackTrace:
                self.lock.acquire()
                try:
                    if label not in self.users:
                        self.users[label] = MemUser(label)

                    if isAlloc:
                        self.users[label].alloc(1)
                    else:
                        self.users[label].free(1)
                finally:
                    self.lock.release()
        finally:
            #process.resume()
            thread.resume()

    def post_HeapAlloc( self, event, retval ):
        return

    def post_HeapFree( self, event, retval ):
        return

    def printLog(self, process):
        self.lock.acquire()
        try:
            for addr, user in self.users.items():
                print "Function: " + user.name + ", alloc=" + str(user.allocTotal) + ", free=" + str(user.freeTotal)
        finally:
            self.lock.release()

def memMonitor_debugger( argv ):

    # Get the process ID from the command line.
    pid = int( argv[0] )

    handler = MyEventHandler()
    process = None

    # Instance a Debug object, passing it the MyEventHandler instance.
    with Debug( handler, bKillOnExit = True ) as debug:
        try:
            # Start a new process for debugging.
            process = debug.attach( pid )

            # Wait for the debugee to finish.
            debug.loop()

        finally:
            debug.detach(pid)
            debug.stop()
            handler.printLog(process)

# When invoked from the command line,
# the first argument is an executable file,
# and the remaining arguments are passed to the newly created process.
if __name__ == "__main__":
    import sys
    memMonitor_debugger( sys.argv[1:] )
