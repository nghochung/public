#pragma once

#include <windows.h>
#include <DbgHelp.h>
#include <intsafe.h>
#include <memory>
#include <vector>
//------------------------------------------------------------------------------- DumpFile

class DumpFile;
typedef std::shared_ptr<DumpFile> DumpFilePtr;

class DumpFile
{
public:
	static DumpFilePtr open(const std::wstring& path);

private:
	DumpFile(const std::wstring& path, UINT32 fileSize, const BYTE* mappedMem);
	void loadInfoFromDump();

public:
	virtual ~DumpFile();

public:
	UINT32 getFileSize() const;

	const BYTE* getMappedMem() const;
	const BYTE* getStream(MINIDUMP_STREAM_TYPE type, UINT32* size = nullptr) const;

	const MINIDUMP_EXCEPTION_STREAM& getException() const;	
	const MINIDUMP_SYSTEM_INFO& getSysInfo() const;

	const std::vector<MINIDUMP_THREAD>& getThreads() const;
	const MINIDUMP_THREAD* getThread(UINT32 threadId) const;

	const std::vector<MINIDUMP_MODULE>& getModules() const;
	const MINIDUMP_MODULE* getModuleHasAddr(UINT32 addr) const;

	const std::vector<MINIDUMP_MEMORY_DESCRIPTOR>& getMemories() const;

	const BYTE* getRvaMemory(RVA rva, int size) const;
	bool getRvaString(RVA rva, std::wstring* str) const;
	bool getRvaContext(RVA rva, CONTEXT* context) const;

	const BYTE* getMemoryPointer(UINT32 addr, INT32 size) const;

	INT32 readMemory(UINT32 addr, INT32 size, BYTE* buffer) const;

	template <typename T> bool readMemoryValue(UINT32 addr, T* value) const
	{
		return readMemory(addr, sizeof(*value), (BYTE*)value) == sizeof(*value);
	}

private:
	std::wstring m_wsPath;
	UINT32 m_ui32FileSize;
	const BYTE* m_pMappedMem;
	MINIDUMP_EXCEPTION_STREAM m_ExceptionStream;
	MINIDUMP_FUNCTION_TABLE_STREAM m_FuncTableStream;
	MINIDUMP_SYSTEM_INFO m_SysInfo;
	std::vector<MINIDUMP_THREAD> m_aThreads;
	std::vector<MINIDUMP_MODULE> m_aModules;
	std::vector<MINIDUMP_MEMORY_DESCRIPTOR> m_aMemoryDesc;
	std::vector<MINIDUMP_FUNCTION_TABLE_DESCRIPTOR> m_aFuncTableDesc;
};