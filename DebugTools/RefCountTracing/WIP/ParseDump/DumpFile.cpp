#include "DumpFile.h"

#include <algorithm>
//------------------------------------------------------------------------------- DumpFile

DumpFilePtr DumpFile::open(const std::wstring& path)
{
	// open the file as a memory mapped form

	HANDLE fileHandle = CreateFile(
		path.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, 
		OPEN_EXISTING, NULL, NULL);
	if (fileHandle == INVALID_HANDLE_VALUE) 
		return nullptr;

	UINT32 fileSize = GetFileSize(fileHandle, NULL);
	if (fileSize == 0)
	{
		CloseHandle(fileHandle);
		return nullptr;
	}

	HANDLE mappingHandle = CreateFileMapping(
		fileHandle, NULL, PAGE_READONLY, NULL, NULL, NULL);
	CloseHandle(fileHandle);
	if (mappingHandle == nullptr) 
		return nullptr;

	const BYTE* mappedMem = (const BYTE*)MapViewOfFile(
		mappingHandle, FILE_MAP_READ, NULL, NULL, NULL);
	CloseHandle(mappingHandle);
	if (mappedMem == nullptr) 
		return nullptr;

	// check header

	if (memcmp(mappedMem, "MDMP", 4) != 0)
	{
		UnmapViewOfFile(mappedMem);
		return nullptr;
	}

	// load stream

	DumpFilePtr dumpFile;
	dumpFile.reset(new DumpFile(path, fileSize, mappedMem));
	return dumpFile;
}

DumpFile::DumpFile(const std::wstring& path, UINT32 fileSize, const BYTE* mappedMem)
	: m_wsPath(path)
	, m_ui32FileSize(fileSize)
	, m_pMappedMem(mappedMem)
{
	loadInfoFromDump();
}

void DumpFile::loadInfoFromDump()
{
	BOOL ret;
	MINIDUMP_DIRECTORY* mdDir;
	VOID* streamPointer;
	ULONG streamSize;

	// load ExceptionStream

	ZeroMemory(&m_ExceptionStream, sizeof(m_ExceptionStream));
	ret = MiniDumpReadDumpStream((PVOID)m_pMappedMem, ExceptionStream, &mdDir, &streamPointer, &streamSize);
	if (ret && IsBadReadPtr(streamPointer, sizeof(MINIDUMP_EXCEPTION_STREAM)) == FALSE) 
		m_ExceptionStream = *(MINIDUMP_EXCEPTION_STREAM*)streamPointer;

	// load SystemInfoStream

	ZeroMemory(&m_SysInfo, sizeof(m_SysInfo));
	ret = MiniDumpReadDumpStream((PVOID)m_pMappedMem, SystemInfoStream, &mdDir, &streamPointer, &streamSize);
	if (ret && IsBadReadPtr(streamPointer, sizeof(MINIDUMP_SYSTEM_INFO)) == FALSE) 
		m_SysInfo = *(MINIDUMP_SYSTEM_INFO*)streamPointer;

	// load ThreadListStream

	ret = MiniDumpReadDumpStream((PVOID)m_pMappedMem, ThreadListStream, &mdDir, &streamPointer, &streamSize);
	if (ret && IsBadReadPtr(streamPointer, sizeof(MINIDUMP_THREAD_LIST)) == FALSE) 
	{
		MINIDUMP_THREAD_LIST* threadList = (MINIDUMP_THREAD_LIST*)streamPointer;
		m_aThreads.reserve(threadList->NumberOfThreads);
		for (int i=0; i < int(threadList->NumberOfThreads); i++)
			m_aThreads.push_back(threadList->Threads[i]);
	}

	// load ModuleListStream

	ret = MiniDumpReadDumpStream((PVOID)m_pMappedMem, ModuleListStream, &mdDir, &streamPointer, &streamSize);
	if (ret && IsBadReadPtr(streamPointer, sizeof(MINIDUMP_MODULE_LIST)) == FALSE) 
	{
		MINIDUMP_MODULE_LIST* moduleList = (MINIDUMP_MODULE_LIST*)streamPointer;
		m_aModules.reserve(moduleList->NumberOfModules);
		for (int i=0; i < int(moduleList->NumberOfModules); i++)
			m_aModules.push_back(moduleList->Modules[i]);
	}

	// load MemoryListStream

	ret = MiniDumpReadDumpStream((PVOID)m_pMappedMem, MemoryListStream, &mdDir, &streamPointer, &streamSize);
	if (ret && IsBadReadPtr(streamPointer, sizeof(MINIDUMP_MEMORY_LIST)) == FALSE) 
	{
		MINIDUMP_MEMORY_LIST* memoryList = (MINIDUMP_MEMORY_LIST*)streamPointer;
		m_aMemoryDesc.reserve(memoryList->NumberOfMemoryRanges);
		for (int i=0; i < int(memoryList->NumberOfMemoryRanges); i++)
		{
			const BYTE* ptrInRange = m_pMappedMem + memoryList->MemoryRanges[i].Memory.Rva;
			UINT32 sizeInRange = memoryList->MemoryRanges[i].Memory.DataSize;
			if (IsBadReadPtr(ptrInRange, sizeInRange) == FALSE)
				m_aMemoryDesc.push_back(memoryList->MemoryRanges[i]);
		}
	}

	ret = MiniDumpReadDumpStream((PVOID)m_pMappedMem, Memory64ListStream, &mdDir, &streamPointer, &streamSize);
	if (ret && IsBadReadPtr(streamPointer, sizeof(MINIDUMP_MEMORY64_LIST)) == FALSE) 
	{
		MINIDUMP_MEMORY64_LIST* memory64List = (MINIDUMP_MEMORY64_LIST*)streamPointer;
		m_aMemoryDesc.reserve((int)memory64List->NumberOfMemoryRanges);

		UINT32 rva = (UINT32)memory64List->BaseRva;
		for (int i=0; i < int(memory64List->NumberOfMemoryRanges); i++)
		{
			MINIDUMP_MEMORY_DESCRIPTOR mem;
			{
				mem.StartOfMemoryRange = memory64List->MemoryRanges[i].StartOfMemoryRange;
				mem.Memory.DataSize = (UINT32)memory64List->MemoryRanges[i].DataSize;
				mem.Memory.Rva = rva;
				rva += mem.Memory.DataSize;
			}
			m_aMemoryDesc.push_back(mem);
		}
	}

	ret = MiniDumpReadDumpStream((PVOID)m_pMappedMem, FunctionTableStream, &mdDir, &streamPointer, &streamSize);
	if (ret && IsBadReadPtr(streamPointer, sizeof(MINIDUMP_MEMORY64_LIST) == FALSE))
	{
		m_FuncTableStream = *(MINIDUMP_FUNCTION_TABLE_STREAM*)streamPointer;
		MINIDUMP_FUNCTION_TABLE_DESCRIPTOR* funcTableDescList = (MINIDUMP_FUNCTION_TABLE_DESCRIPTOR*)( (MINIDUMP_FUNCTION_TABLE_STREAM*)streamPointer + 1 );
		m_aFuncTableDesc.reserve(m_FuncTableStream.NumberOfDescriptors);

		for (int i=0; i < int(m_FuncTableStream.NumberOfDescriptors); i++)
			m_aFuncTableDesc.push_back( funcTableDescList[i] );
	}

	std::sort(m_aMemoryDesc.begin(), m_aMemoryDesc.end(), 
		[](const MINIDUMP_MEMORY_DESCRIPTOR& a, const MINIDUMP_MEMORY_DESCRIPTOR& b) -> bool 
		{ return a.StartOfMemoryRange < b.StartOfMemoryRange; });
}

DumpFile::~DumpFile()
{
	UnmapViewOfFile(m_pMappedMem);
	m_pMappedMem = nullptr;
}

UINT32 DumpFile::getFileSize() const
{
	return m_ui32FileSize;
}

const BYTE* DumpFile::getMappedMem() const
{
	return m_pMappedMem;
}

const BYTE* DumpFile::getStream(MINIDUMP_STREAM_TYPE type, UINT32* size) const
{
	MINIDUMP_DIRECTORY* mdDir;
	VOID* streamPointer;
	ULONG streamSize;

	BOOL ret = MiniDumpReadDumpStream((PVOID)m_pMappedMem, type, &mdDir, &streamPointer, &streamSize);
	if (ret == FALSE)
		return nullptr;

	if (size != nullptr)
		*size = (UINT32)streamSize;

	return (const BYTE*)streamPointer;
}

const MINIDUMP_EXCEPTION_STREAM& DumpFile::getException() const
{
	return m_ExceptionStream;
}

const MINIDUMP_SYSTEM_INFO& DumpFile::getSysInfo() const
{
	return m_SysInfo;
}

const std::vector<MINIDUMP_THREAD>& DumpFile::getThreads() const
{
	return m_aThreads;
}

const MINIDUMP_THREAD* DumpFile::getThread(UINT32 threadId) const
{
	for (auto t = m_aThreads.begin(), t_end = m_aThreads.end(); t != t_end; t += 1)
	{
		if (t->ThreadId == threadId)
			return &*t;
	}
	return nullptr;
}

const std::vector<MINIDUMP_MODULE>& DumpFile::getModules() const
{
	return m_aModules;
}

const MINIDUMP_MODULE* DumpFile::getModuleHasAddr(UINT32 addr) const
{
	for (auto m = m_aModules.begin(), m_end = m_aModules.end(); m != m_end; m += 1)
	{
		if (addr >= m->BaseOfImage && addr < m->BaseOfImage + m->SizeOfImage)
			return &*m;
	}
	return nullptr;
}

const std::vector<MINIDUMP_MEMORY_DESCRIPTOR>& DumpFile::getMemories() const
{
	return m_aMemoryDesc;
}

const BYTE* DumpFile::getRvaMemory(RVA rva, int size) const
{
	const BYTE* buf = m_pMappedMem + rva;
	if (IsBadReadPtr(buf, size) != FALSE)
		return nullptr;

	return buf;
}

bool DumpFile::getRvaString(RVA rva, std::wstring* str) const
{
	const MINIDUMP_STRING* mstr = (const MINIDUMP_STRING*)(m_pMappedMem + rva);
	if (IsBadReadPtr(mstr->Buffer, mstr->Length) != FALSE)
		return false;

	str->resize(mstr->Length / 2);
	memcpy(&((*str)[0]), mstr->Buffer, mstr->Length);
	return true;
}

bool DumpFile::getRvaContext(RVA rva, CONTEXT* context) const
{
	const CONTEXT* ctx = (const CONTEXT*)(m_pMappedMem + rva);
	if (IsBadReadPtr(ctx, sizeof(CONTEXT)) != FALSE)
		return false;

	*context = *ctx;
	return true;
}

const BYTE* DumpFile::getMemoryPointer(UINT32 addr, INT32 size) const
{
	// find a memory item which contains curAddr

	MINIDUMP_MEMORY_DESCRIPTOR value;
	value.StartOfMemoryRange = addr;
	auto i = lower_bound(m_aMemoryDesc.begin(), m_aMemoryDesc.end(), value,
		[](const MINIDUMP_MEMORY_DESCRIPTOR& a, const MINIDUMP_MEMORY_DESCRIPTOR& b) -> bool 
	{ return a.StartOfMemoryRange <= b.StartOfMemoryRange; });
	if (i == m_aMemoryDesc.begin())
		return nullptr;
	i--;

	INT32 posInRange = addr - UINT32(i->StartOfMemoryRange);
	INT32 availableInRange = i->Memory.DataSize - posInRange;
	if (availableInRange <= size)
		return nullptr;

	return m_pMappedMem + (i->Memory.Rva + posInRange);
}

INT32 DumpFile::readMemory(UINT32 addr, INT32 size, BYTE* buffer) const
{
	UINT32 curAddr = addr;
	INT32 leftSize = size;

	while (leftSize > 0)
	{
		// find a memory item which contains curAddr

		MINIDUMP_MEMORY_DESCRIPTOR value;
		value.StartOfMemoryRange = curAddr;
		auto i = lower_bound(m_aMemoryDesc.begin(), m_aMemoryDesc.end(), value,
			[](const MINIDUMP_MEMORY_DESCRIPTOR& a, const MINIDUMP_MEMORY_DESCRIPTOR& b) -> bool 
		{ return a.StartOfMemoryRange <= b.StartOfMemoryRange; });
		if (i == m_aMemoryDesc.begin())
			break;
		i--;

		// copy memory from item found

		INT32 posInRange = curAddr - UINT32(i->StartOfMemoryRange);
		INT32 availableInRange = i->Memory.DataSize - posInRange;
		if (availableInRange <= 0)
			break;

		if (leftSize <= availableInRange)
		{
			memcpy(
				buffer + (curAddr - addr), 
				m_pMappedMem + (i->Memory.Rva + posInRange), 
				leftSize);
			curAddr += leftSize;
			leftSize = 0;
		}
		else
		{
			memcpy(
				buffer + (curAddr - addr), 
				m_pMappedMem + (i->Memory.Rva + posInRange), 
				availableInRange);
			curAddr += availableInRange;
			leftSize -= availableInRange;
		}
	}

	return size - leftSize;
}