<!-- <?xml version="1.0" encoding="UTF-16"?> -->
<instrumentationManifest
		xmlns="http://schemas.microsoft.com/win/2004/08/events"
		xmlns:win="http://manifests.microsoft.com/win/2004/08/windows/events"
		xmlns:xs="http://www.w3.org/2001/XMLSchema"
		>
  <!--
// {66A89001-15CC-46E0-AAB4-C668BB64E294}
DEFINE_GUID(<<name>>, 
0x66a89001, 0x15cc, 0x46e0, 0xaa, 0xb4, 0xc6, 0x68, 0xbb, 0x64, 0xe2, 0x94);
-->
  <instrumentation>
    <events>

      <provider name="xsiProvider"
          guid="{66A89001-15CC-46E0-AAB4-C668BB64E294}"
          symbol="xsiProviderGuid"
          resourceFileName="d:\build\bin\leakProgram.exe"
          messageFileName="d:\build\bin\leakProgram.exe"
          message="$(string.Provider.Name)"
								>

        <keywords>
          <keyword name="Read" symbol="READ_KEYWORD" mask="0x1" />
          <keyword name="Write" symbol="WRITE_KEYWORD" mask="0x2" />
          <keyword name="Local" symbol="LOCAL_KEYWORD" mask="0x4" />
          <keyword name="Remote" symbol="REMOTE_KEYWORD" mask="0x8" />
        </keywords>

        <templates>
          <template tid="CompactTraceInfoTemplate">
            <data name="Addr" inType="win:Pointer"/>
            <data name="StackDepth" inType="win:UInt32" />
            <data name="PCAddr" count="StackDepth" inType="win:UInt64" />
          </template>
          <template tid="TraceInfoTemplate">
            <data name="Addr" inType="win:Pointer"/>
            <data name="StackDepth" inType="win:UInt32" />
            <data name="StackTrace" count="StackDepth" inType="win:Binary" length="264"/>
          </template>
          <template tid="PyTraceInfoTemplate">
            <data name="Addr" inType="win:Pointer"/>
            <data name="PyTrace" inType="win:UnicodeString" />
			<data name="StackDepth" inType="win:UInt32" />
			<data name="StackItemSize" inType="win:UInt32" />
            <data name="Trace" count="StackDepth" inType="win:Binary" length="264"/>
          </template>
        </templates>

        <events>
          <event value="1"
              level="win:Informational"
              template="TraceInfoTemplate"
              symbol="CreateInstanceEvent"
              message ="$(string.Event.CreateInstance)"
              keywords="Read Local" />
          <event value="2"
						level="win:Informational"
						template="TraceInfoTemplate"
						symbol="DestroyInstanceEvent"
						message ="$(string.Event.DestroyInstance)"
						keywords="Read Local" />
          <event value="3"
						level="win:Informational"
						template="CompactTraceInfoTemplate"
						symbol="AddRefEvent"
						message ="$(string.Event.AddRef)"
						keywords="Read Local" />
          <event value="4"
						level="win:Informational"
						template="CompactTraceInfoTemplate"
						symbol="DecRefEvent"
						message ="$(string.Event.DecRef)"
						keywords="Read Local" />

        </events>
      </provider>
    </events>

  </instrumentation>

  <localization>
    <resources culture="en-US">
      <stringTable>

        <string id="Provider.Name" value="xsiProvider"/>
        <string id="Event.CreateInstance" value="Instance %0 created, backtrace: %1"/>
        <string id="Event.DestroyInstance" value="Instance %0 destroy, back trace: %1"/>
        <string id="Event.AddRef" value="Instance %0 addref"/>
        <string id="Event.DecRef" value="Instance %0 decref"/>
      </stringTable>
    </resources>
  </localization>

</instrumentationManifest>
